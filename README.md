# streambrain_vencabot
## Description
This module provides the Streambrain, Handler, Listener, and Event objects which are used for my custom event-handling system. It's super-simple on its own and is meant to be extended with packages of Listeners for various APIs, etc.

## Status
It's a very simple script and I'd say that it's mostly complete. I might want to add some features including a standardized 'TimerListener' which just spawns an event after a certain amount of time has passed.

## License
This module is licensed under the [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
